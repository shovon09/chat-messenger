/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatserver;

/**
 *
 * @author Shovon
 */
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataBase {

    private final String URL = "jdbc:mysql://localhost/mesenger";
    private Connection connection = null;
    private PreparedStatement selectUser = null;
    private PreparedStatement insertUser = null;

    public DataBase() {
        try {
            connection = DriverManager.getConnection(URL, "root", "");
            selectUser = connection.prepareStatement("SELECT * FROM `users` WHERE `email`= ?");
            insertUser = connection.prepareStatement("INSERT INTO `mesenger`.`users`" + "(`userid`, `name`, `email`, `password`,division,fullAddress,contactNumber,aboutUser)"
                    + "VALUES (NULL,?,?,?,?,?,?,?);");
        } catch (SQLException ex) {
            // Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String verifyUser(String email, String password) {
        ResultSet result = null;
        String name = null;
        String pass = null;
        String mail = null;
        String verification = "NVR" + " ";
        try {
            selectUser.setString(1, email.trim());
            result = selectUser.executeQuery();

            while (result.next()) {
                name = result.getString("name");
                mail = result.getString("email");
                pass = result.getString("password");

                if (mail.equalsIgnoreCase(email) && pass.equalsIgnoreCase(password)) {
                    verification = "UOK" + " " + name;
                } else {
                    verification = "NVR" + " ";
                }
            }

        } catch (SQLException ex) {
            //  Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                result.close();
            } catch (SQLException ex) {
                // Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return verification;


    }
    public int insertAUser(String name,String email,String password,String division,String fullAddress,String contactNumber,String aboutUser){
        int result = 0;
        try {
            insertUser.setString(1,name);
            insertUser.setString(2,email);
            insertUser.setString(3,password);
            insertUser.setString(4, division);
            insertUser.setString(5, fullAddress);
            insertUser.setString(6, contactNumber);
            insertUser.setString(7, aboutUser);
            result = insertUser.executeUpdate();
        } catch (SQLException ex) {
            //Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return  result;
        
    }
    
}
