/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatserver;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.io.*;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import sun.audio.AudioPlayer;

/**
 *
 * @author Shovon
 */
public class ChatServer implements Runnable {

    private String port;
    private int controlPort;
    private int filePort;
    ServerWindow serverWindow;
    ServerSocket serverSocket;
    ServerSocket controlSocket;
    ServerSocket fileSocket;
    private ArrayList<Socket> clientSockets;
    private ArrayList<Socket> controlSockets;
    private ArrayList<Socket> fileSockets;
    private ArrayList<Color> assignedFontColor;
    private final int SOCKET_TIMEOUT_IN_MS = 0;
    private String userName = "USR";
    DataBase userdb;

    public ChatServer(String portnumber) {
        port = portnumber;
        // controlPort = Integer.parseInt(port);
        controlPort = 55555;
        filePort = 7878;
        new Thread(this).start();
        userdb = new DataBase();
    }

    @Override
    public void run() {
        clientSockets = new ArrayList<Socket>();
        controlSockets = new ArrayList<Socket>();
        fileSockets = new ArrayList<Socket>();
        assignedFontColor = new ArrayList<Color>();
        serverWindow = new ServerWindow(this);
        serverWindow.setVisible(true);
    }

    public void startServer() {

        new SocketListening(this);
        new ControlSocketListening(this);
        new FileSocketListening(this);

    }

    void clientExited(Socket clientSocket) {
        for (int i = 0; i < clientSockets.size(); i++) {
            if (clientSocket == clientSockets.get(i)) {
                clientSockets.remove(i);
                break;
            }
        }

        for (int i = 0; i < controlSockets.size(); i++) {
            if (clientSocket == controlSockets.get(i)) {
                controlSockets.remove(i);
                break;
            }
        }
    }

    synchronized void newMsgReceivedFromClient(String msg, Socket fromSocket) {

        int indexOfFromSocket = clientSockets.indexOf(fromSocket);
        Color assignedColor = assignedFontColor.get(indexOfFromSocket);
        msg = "<font color ='#" + Integer.toHexString(assignedColor.getRed())
                + Integer.toHexString(assignedColor.getGreen())
                + Integer.toHexString(assignedColor.getBlue())
                + "'>" + msg + "</font>";

        serverWindow.setScreenText(msg);

        /// talkin to other client in the room....///
        for (Socket clientSocket : clientSockets) {
            if (clientSocket == fromSocket) {
                continue;
            }
            try {
                PrintStream outputStream = new PrintStream(clientSocket.getOutputStream());
                outputStream.println(msg);
                outputStream.flush();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(serverWindow, e.toString(),
                        "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    synchronized void processControlMessage(String msg, Socket fromSocket) {
        /// control msg process korar jonno ei class .... /// 

        if (msg.regionMatches(0, "USR", 0, 3)) {

            serverWindow.setScreenText(msg.substring(4) + " joined" + "<br/>");
            userName = userName + msg.substring(4) + ".";
            System.out.println(userName);
            sendControlMsg(userName, fromSocket);

        } else if (msg.regionMatches(0, "VER", 0, 3)) {
            String email;
            String password;
            String[] msgsplit = msg.split(" ");
            email = msgsplit[1];
            password = msgsplit[2];
            String retval = userdb.verifyUser(email, password);
            System.out.println(retval);
            sendVerification(retval, fromSocket);

        } // System.out.println(msg.substring(0, 3));
        else if (msg.regionMatches(0, "DIN", 0, 3)) {
            String confirmation = "IFL ";
            String myname;
            String email;
            String password;
            String[] msgsplit = msg.split("#");
            myname = msgsplit[1];
            email = msgsplit[2];
            password = msgsplit[3];
            String division = msgsplit[4];
            String fullAddress = msgsplit[5];
            String contactNumber = msgsplit[6];
            String aboutUser = msgsplit[7];
            int retval = userdb.insertAUser(myname, email, password,division,fullAddress,contactNumber,aboutUser);
            System.out.println(retval);
            if (retval > 0) {
                confirmation = "ISC ";
                try {
                    SendEmail.sendConfirmationEmail("","", email, password, "", "");
                } catch (MessagingException ex) {
                    Logger.getLogger(ChatServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            sendVerification(confirmation, fromSocket);

        }


    }

    void sendControlMsg(String msg, Socket fromSocket) {
        for (Socket clientSocket : controlSockets) {
            if (clientSocket == fromSocket) {
                // continue;
            }
            try {
                PrintStream outputStream = new PrintStream(clientSocket.getOutputStream());
                outputStream.println(msg);
                outputStream.flush();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(serverWindow, e.toString(),
                        "Error Balll", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    void sendVerification(String msg, Socket fromSocket) {
        for (Socket clientSocket : controlSockets) {
            if (clientSocket == fromSocket) {
                try {
                    PrintStream outputStream = new PrintStream(clientSocket.getOutputStream());
                    outputStream.println(msg);
                    outputStream.flush();

                } catch (IOException e) {
                    JOptionPane.showMessageDialog(serverWindow, e.toString(),
                            "Error in Verification", JOptionPane.ERROR_MESSAGE);
                }
            }

        }
    }

    synchronized void sendFile(String msg, Socket fromSocket) {
        for (Socket fileSocket : fileSockets) {
            if (fileSocket == fromSocket) {
                continue;
            }
            try {
                PrintStream outputStream = new PrintStream(fileSocket.getOutputStream());
                outputStream.println(msg);
                outputStream.flush();
                System.out.println(msg);

            } catch (IOException e) {
                JOptionPane.showMessageDialog(serverWindow, e.toString(),
                        "Error File Sending", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    void freePortAndExit() {
        try {
            serverSocket.close();
            for (Socket socket : clientSockets) {
                socket.close();
            }
            controlSocket.close();
            for (Socket socket : controlSockets) {
                socket.close();
            }

        } catch (Exception e) {
        } finally {
            System.exit(0);
        }
    }

    class SocketListening implements Runnable {

        ChatServer server;

        SocketListening(ChatServer server) {
            this.server = server;
            new Thread(this).start();
        }

        public void run() {
            try {

                serverSocket = new ServerSocket(Integer.parseInt(port));

                while (true) {

                    Socket client = serverSocket.accept();
                    client.setSoTimeout(SOCKET_TIMEOUT_IN_MS);
                    clientSockets.add(client);
                    System.out.println(client);
                    assignedFontColor.add(new Color(new Random().nextInt(200),
                            new Random().nextInt(200),
                            new Random().nextInt(200)));
                    ClientSocket clientSocket = new ClientSocket(client, server);
                }
            } catch (SocketTimeoutException e) {
            } catch (SocketException e) {
            } catch (Exception e) {
                JOptionPane.showMessageDialog(serverWindow, e.toString(),
                        "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    class ControlSocketListening implements Runnable {

        ChatServer server;

        ControlSocketListening(ChatServer server) {
            this.server = server;
            new Thread(this).start();
        }

        public void run() {
            try {
                controlSocket = new ServerSocket(controlPort);
                while (true) {

                    Socket controlClient = controlSocket.accept();
                    controlClient.setSoTimeout(SOCKET_TIMEOUT_IN_MS);
                    System.out.println(controlClient);
                    controlSockets.add(controlClient);
                    ClientControlSocket clientControl = new ClientControlSocket(controlClient, server);

                }
            } catch (SocketTimeoutException e) {
            } catch (SocketException e) {
            } catch (Exception e) {
                JOptionPane.showMessageDialog(serverWindow, e.toString(),
                        "Error in ControlSocketListening", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    class FileSocketListening implements Runnable {

        ChatServer server;

        public FileSocketListening(ChatServer server) {
            this.server = server;
            new Thread(this).start();
        }

        @Override
        public void run() {
            try {
                fileSocket = new ServerSocket(filePort);
                while (true) {

                    Socket clientFileSocket = fileSocket.accept();
                    clientFileSocket.setSoTimeout(SOCKET_TIMEOUT_IN_MS);
                    System.out.println(clientFileSocket);
                    fileSockets.add(clientFileSocket);
                    ClientFileSocket clientFile = new ClientFileSocket(clientFileSocket, server);

                }
            } catch (SocketTimeoutException e) {
            } catch (SocketException e) {
            } catch (Exception e) {
                JOptionPane.showMessageDialog(serverWindow, e.toString(),
                        "Error in FileSocketListening", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}

class ClientSocket implements Runnable {

    Thread t;
    Socket clientSocket;
    ChatServer server;

    ClientSocket(Socket clientSocket, ChatServer server) {
        this.clientSocket = clientSocket;
        this.server = server;
        t = new Thread(this);
        t.start();
    }

    public void run() {
        try {
            while (true) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        clientSocket.getInputStream()));
                String msg = br.readLine();
                server.newMsgReceivedFromClient(msg, clientSocket);
            }
        } catch (SocketException e) {
            server.clientExited(clientSocket);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(server.serverWindow, e.toString(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}

class ClientControlSocket implements Runnable {

    ChatServer server;
    Socket controlSocket;
    Thread t;

    public ClientControlSocket(Socket controlSocket, ChatServer server) {
        this.controlSocket = controlSocket;
        this.server = server;
        t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        try {
            while (true) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        controlSocket.getInputStream()));
                String controlmsg = br.readLine();
                server.processControlMessage(controlmsg, controlSocket);
            }
        } catch (SocketException e) {
            server.clientExited(controlSocket);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(server.serverWindow, e.toString(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }

    }
}

class ClientFileSocket implements Runnable {

    ChatServer server;
    Socket fileSocket;
    Thread t;

    public ClientFileSocket(Socket fileSocket, ChatServer server) {
        this.fileSocket = fileSocket;
        this.server = server;
        t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        try {
            while (true) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        fileSocket.getInputStream()));
                String fileLine;
                while ((fileLine = br.readLine()) != null) {

                    server.sendFile(fileLine, fileSocket);
                    System.out.println(fileLine);
                }
                //  br.close();


            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(server.serverWindow, ex.toString(),
                    "Error in File Reading", JOptionPane.ERROR_MESSAGE);
        }
    }
}
