/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatclient;

/**
 *
 * @author Shovon
 */
import java.io.*;
import java.net.*;

public class ClientInfoAndControlMsg implements Runnable{
    private String name =null;
    private Socket socket = null ;
    private String email= null;
    private String password = null ;
    private String division = null;
    private String fullAddress = null;
    private String contactNumber = null;
    private String aboutUser = null;



   

    public ClientInfoAndControlMsg(String name , Socket socket , String email , String password,String division,String fullAddress,String contactNumber,String aboutUser) {
        this.name = name ;
        this.socket = socket;
        this.email = email;
        this.password = password;
        this.division = division;
        this.fullAddress= fullAddress;
        this.contactNumber = contactNumber;
        this.aboutUser = aboutUser;
    }
    public ClientInfoAndControlMsg(){
        
    }
    
        public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAboutUser() {
        return aboutUser;
    }

    public void setAboutUser(String aboutUser) {
        this.aboutUser = aboutUser;
    }
    
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
       return name ;
    }
    
     public void setSocket(Socket socket){
        this.socket = socket;
    }
    public Socket getSocket(){
       return socket ;
    }
    public void setEmail (String email){
        this.email = email;
        
    }
    public String getEmail(){
        return email;
    }
    public void setPassword(String password){
        this.password = password ;
    }
    public String getPassword () {
        return password ;
    }
    
    //// CONTROL MESSEGES ...///
    public String sendUserName (){
        String msg = "USR" + " " + name;
        return msg;
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    
}
