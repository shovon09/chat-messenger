package chatclient;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEmail {

    public static boolean sendEmail(String fromEmail, String password, String toEmail, String subject, String content) throws MessagingException {

        String host = "smtp.gmail.com";
        String from = "imshovon@gmail.com";
        String pass = "sh1094009";

        if (!password.isEmpty()) {
            pass = password;
        }
        if (!fromEmail.isEmpty()) {
            from = fromEmail;
        }


        Properties props = System.getProperties();
        props.put("mail.smtp.starttls.enable", "true"); // added this line
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        String[] to = {toEmail}; // added this line

        Session session = Session.getDefaultInstance(props, null);
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));

        InternetAddress[] toAddress = new InternetAddress[to.length];

        // To get the array of addresses
        for (int i = 0; i < to.length; i++) { // changed from a while loop
            toAddress[i] = new InternetAddress(to[i]);
        }


        for (int i = 0; i < toAddress.length; i++) { // changed from a while loop
            message.addRecipient(Message.RecipientType.TO, toAddress[i]);
        }
        if (fromEmail.isEmpty()) {
            message.setSubject("Registration Confirmation");

            message.setContent("<h1>Thank you for registering.</h1>"
                    + "</br>" + "<p> Your Email and password is  : " + content,
                    "text/html");
        } else {
            message.setSubject(subject);

            message.setContent(content, "text/html");
        }


        Transport transport = session.getTransport("smtp");
        transport.connect(host, from, pass);
        System.out.println("Trying to send email");
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
        System.out.println("done");
        return true;

    }

    public static void sendConfirmationEmail(String fromEmail, String password, String toEmail, String loginPassword, String subject, String content) throws MessagingException {

        String host = "smtp.gmail.com";
        String from = "imshovon@gmail.com";
        String pass = "sh1094009";

        if (!password.isEmpty()) {
            pass = password;
        }
        if (!fromEmail.isEmpty()) {
            from = fromEmail;
        }


        Properties props = System.getProperties();
        props.put("mail.smtp.starttls.enable", "true"); // added this line
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        String[] to = {toEmail}; // added this line

        Session session = Session.getDefaultInstance(props, null);
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));

        InternetAddress[] toAddress = new InternetAddress[to.length];

        // To get the array of addresses
        for (int i = 0; i < to.length; i++) { // changed from a while loop
            toAddress[i] = new InternetAddress(to[i]);
        }


        for (int i = 0; i < toAddress.length; i++) { // changed from a while loop
            message.addRecipient(Message.RecipientType.TO, toAddress[i]);
        }
        if (fromEmail.isEmpty()) {
            message.setSubject("Registration Confirmation");

            message.setContent("<h1>Thank you for registering.</h1>"
                    + "</br>" + "<p> Your password is  : " + loginPassword + "</br>" + "Your mail id is : " + toEmail,
                    "text/html");
        } else {
            message.setSubject(subject);

            message.setContent(content, "text/html");
        }


        Transport transport = session.getTransport("smtp");
        transport.connect(host, from, pass);
        System.out.println("Trying to send email");
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
        System.out.println("done");

    }
}
