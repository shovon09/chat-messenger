/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatclient;

/**
 *
 * @author Shovon
 */
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class ChatClient {

    private String serverIP;
    private String serverPort;
    private int controlPort;
    private int fileport;
    private Socket socket;
    private Socket controlSocket;
    private Socket filesocket;
    private String email;
    private String password;
    private String myname;
    private String division;
    private String fullAddress;
    private String contactNumber;
    private String aboutUser;
    ChatWindow clientWindow;
    ClientInfoAndControlMsg clientInfoAndControlMsg;
    JFileChooser filechooser;
    StartClient startWindow;

    public ChatClient(String serverIP, String serverPort, String username) {
        this.serverIP = serverIP;
        this.serverPort = serverPort;
        fileport = 7878;
        controlPort = 55555;
        clientWindow = new ChatWindow(username, this);
        clientInfoAndControlMsg = new ClientInfoAndControlMsg();
        clientInfoAndControlMsg.setName(username);
        filechooser = new JFileChooser();

        clientWindow.setVisible(true);
    }

    public ChatClient(String serverIP, String email, String password, StartClient startWindow) {
        this.serverIP = serverIP;
        controlPort = 55555;
        this.email = email;
        this.password = password;
        this.startWindow = startWindow;
        clientInfoAndControlMsg = new ClientInfoAndControlMsg();
    }

    public ChatClient(String serverIP, String name, String email, String password,String division,String fullAddress,String contactNumber,String aboutUser,StartClient startWindow) { // for insertion
        this.serverIP = serverIP;
        controlPort = 55555;
        myname = name;
        this.email = email;
        this.password = password;
        this.division = division;
        this.fullAddress = fullAddress;
        this.contactNumber = contactNumber;
        this.aboutUser = aboutUser;
        this.startWindow = startWindow;
        clientInfoAndControlMsg = new ClientInfoAndControlMsg();
    }

    public void start() {
        try {
            socket = new Socket(serverIP, Integer.parseInt(serverPort));
            System.out.println(socket);
            new ClientSocket(socket, this);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(clientWindow, e.toString(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void controlStart() {

        try {
            controlSocket = new Socket(serverIP, controlPort);
            System.out.println(controlSocket);
            new ControlSocket(controlSocket, this);
            String contrlmsg;
            String name;
            if ((name = clientInfoAndControlMsg.getName()) != null) {  // nam pore database chk er por set kore dite hobe ...//
                contrlmsg = clientInfoAndControlMsg.sendUserName();
                sendControlSignalToServer(contrlmsg);
            } else {
                contrlmsg = "VER" + " " + email + " " + password; /// Database Verification pawar por socket ta close kore dite hobe...

                sendControlSignalToServer(contrlmsg);
                System.out.println(contrlmsg);
            }



        } catch (Exception e) {
            JOptionPane.showMessageDialog(clientWindow, e.toString(),
                    "Error in Verification", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void controlMessageForDBinsert() {

        try {
            controlSocket = new Socket(serverIP, controlPort);
            System.out.println(controlSocket);
            new ControlSocket(controlSocket, this);
            String contrlmsg;
            String name;

            contrlmsg = "DIN" + "#" + myname  +"#" + email +"#" + password + "#"  + division +"#" + fullAddress + "#"+ contactNumber + "#" +aboutUser ; /// Database Verification pawar por socket ta close kore dite hobe...

            sendControlSignalToServer(contrlmsg);
            System.out.println(contrlmsg);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(clientWindow, e.toString(),
                    "Error DB Insertion", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void fileSocketStart() {
        try {
            filesocket = new Socket(serverIP, fileport);
            System.out.println(filesocket);
            new FileSocketListening(filesocket, this);



        } catch (Exception e) {
            JOptionPane.showMessageDialog(clientWindow, e.toString(),
                    "Error in Opening File Socket", JOptionPane.ERROR_MESSAGE);
        }
    }

    void newMsgReceivedFromOthers(String msg) {


        clientWindow.setScreenText(msg);


        System.out.println(msg);
    }

    void sendMsgToOthers(String msg) {

        try {
            PrintStream outputStream = new PrintStream(socket.getOutputStream());
            outputStream.println(msg);
            outputStream.flush();

        } catch (IOException e) {
            JOptionPane.showMessageDialog(clientWindow, e.toString(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    void procesControlSignal(String cntrlmsg) {
        /// Action to do after geting the control signal ... /
        System.out.println(cntrlmsg);
        if (cntrlmsg.regionMatches(0, "USR", 0, 3)) {
            String names = cntrlmsg.substring(3);
            System.out.println(names);
            clientWindow.setUserList(names, clientInfoAndControlMsg.getName());
        } // Database verifiataion pailei control socket close kore dite hobe 
        // verifiation ok hoile username set kore dite hobe /// 
        else if (cntrlmsg.regionMatches(0, "UOK", 0, 3)) {
            String name = cntrlmsg.substring(3);
            name = name.trim();
            System.out.println(name);


            startWindow.loginConfirmed(name);

        } else if (cntrlmsg.regionMatches(0, "NVR", 0, 3)) {

            startWindow.loginNotConfirmed();

        }
        else if (cntrlmsg.regionMatches(0, "IFL", 0, 3)) {
            try {
                controlSocket.close();
            } catch (IOException ex) {
              //  Logger.getLogger(ChatClient.class.getName()).log(Level.SEVERE, null, ex);
            }

            JOptionPane.showMessageDialog(startWindow,"Failed To insert in the database",
                    "Database Error", JOptionPane.ERROR_MESSAGE);

        }
        else if (cntrlmsg.regionMatches(0, "ISC", 0, 3)) {
            try {
                
                controlSocket.close();
            } catch (IOException ex) {
              //  Logger.getLogger(ChatClient.class.getName()).log(Level.SEVERE, null, ex);
            }

           JOptionPane.showMessageDialog(startWindow,"Congratulation you have been registered.You can Login now",
                    "Registration Complete", JOptionPane.INFORMATION_MESSAGE);

        }

    }

    void sendControlSignalToServer(String cntrlmsg) {
        try {
            PrintStream outputStream = new PrintStream(controlSocket.getOutputStream());
            outputStream.println(cntrlmsg);
            outputStream.flush();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(clientWindow, e.toString(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    void sendFileToOthers(File file) {
        try {
            BufferedReader fileReader = new BufferedReader(new FileReader(file));
            PrintStream outputStream = new PrintStream(filesocket.getOutputStream());
            String line;
            while ((line = fileReader.readLine()) != null) {

                outputStream.println(line);
                outputStream.flush();
                System.out.println(line);
            }
            fileReader.close();
            System.out.println("File Read finish");
            // outputStream.close();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(clientWindow, ex.toString(),
                    "Error in FIle Write", JOptionPane.ERROR_MESSAGE);
        }


    }
}

class ClientSocket implements Runnable {

    Thread t;
    Socket clientSocket;
    ChatClient client;

    ClientSocket(Socket clientSocket, ChatClient client) {
        this.clientSocket = clientSocket;
        this.client = client;
        t = new Thread(this);
        t.start();
    }

    public void run() {
        try {
            while (true) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        clientSocket.getInputStream()));
                String msg = br.readLine();
                client.newMsgReceivedFromOthers(msg);
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(client.clientWindow, e.toString(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}

class ControlSocket implements Runnable {

    Thread t;
    Socket controSocket;
    ChatClient client;

    ControlSocket(Socket controSocket, ChatClient client) {
        this.controSocket = controSocket;
        this.client = client;
        t = new Thread(this);
        t.start();
    }

    public void run() {
        try {
            while (true) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        controSocket.getInputStream()));
                String msg = br.readLine();
                client.procesControlSignal(msg);
            }
        } catch (SocketException ex) {
            try {
                controSocket.close();
            } catch (IOException ex1) {
                Logger.getLogger(ControlSocket.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(client.clientWindow, e.toString(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}

class FileSocketListening implements Runnable {

    Thread t;
    ChatClient client;
    Socket fileSocket;

    public FileSocketListening(Socket fileSocket, ChatClient client) {
        this.fileSocket = fileSocket;
        this.client = client;
        t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        try {
            while (true) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        fileSocket.getInputStream()));

                if (br.ready()) {

                    int choice;
                    choice = JOptionPane.showConfirmDialog(client.clientWindow, "Would You Like to Save The File ?", "Warning!!", JOptionPane.YES_NO_OPTION);
                    if (choice == JOptionPane.YES_OPTION) {
                        int confirmation = client.filechooser.showSaveDialog(client.clientWindow);
                        if (confirmation == JFileChooser.APPROVE_OPTION) {
                            File file = client.filechooser.getSelectedFile();
                            PrintWriter fileWriter = new PrintWriter(new FileWriter(file));
                            String line;
                            try {
                                while ((line = br.readLine()) != null) {


                                    fileWriter.println(line);
                                    System.out.println(line);
                                    if (!br.ready()) {
                                        break;
                                    }
                                }
                            } finally {
                                fileWriter.close();
                                // System.out.println("Ball");
                            }


                        } else { // file pointer last e nia jaowar jonno 
                            System.out.println("File not Saved");
                            String line;
                            while ((line = br.readLine()) != null) {
                                if (!br.ready()) {
                                    break;
                                }
                                //  System.out.println("Ball");
                            }

                        }

                    } else { // file pointer last e nia jaowar jonno ...
                        System.out.println("File not accepted");
                        String line;
                        while ((line = br.readLine()) != null) {
                            if (!br.ready()) {
                                break;
                            }
                            //System.out.println("Ball");
                        }
                    }


                }

            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(client.clientWindow, e.toString(),
                    "Error in listening", JOptionPane.ERROR_MESSAGE);
        }

    }
}
